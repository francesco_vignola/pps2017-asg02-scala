package u03lab.code.task1

object Compose extends App {

  val compose = (f: Int => Int, g: Int => Int) => (a: Int) => f(g(a))

  val compose1: (Int => Int, Int => Int) => Int => Int = (f, g) => (a: Int) => f(g(a))

  def compose2(f: Int => Int, g: Int => Int): Int => Int = (a: Int) => f(g(a))

  def compose3(f: Int => Int, g: Int => Int)(a: Int): Int = f(g(a))

  println(compose(_ + 1, _ * 2)(5)) // 11
  println(compose1(_ + 1, _ * 2)(5)) // 11
  println(compose2(_ + 1, _ * 2)(5)) // 11
  println(compose3(_ + 1, _ * 2)(5)) // 11

}
