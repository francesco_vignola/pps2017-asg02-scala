package u04lab.code

sealed trait Option[A] {
  def isEmpty: Boolean = this != None()

  def getOrElse[B >: A](orElse: B): B = this match {
    case Some(a) => a
    case _ => orElse
  }

  def filter(f: A => Boolean): Option[A] = this match {
    case Some(a) if f(a) => this
    case _ => None()
  }

  def map(f: A => Boolean): Option[Boolean] = this match {
    case Some(a) if f(a) => Option(true)
    case Some(_) => Option(false)
    case _ => None()
  }

  def map2[B, C](second: Option[B])(f: (A, B) => C): Option[C] = (this, second) match {
    case (Some(a), Some(b)) => Option(f(a, b))
    case _ => None()
  }

}

case class None[A]() extends Option[A]
case class Some[A](a: A) extends Option[A]


object Option {
  def apply[A](): Option[A] = None()

  def apply[A](a: A): Option[A] = Some(a)
}

object OptionTest extends App {
  println(Option(4))
  println(Option())
  println(Option("ciao"))

  println(Some(4))
  println(None())
  println(Some("ciao"))
}
