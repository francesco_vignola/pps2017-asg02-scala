package u03lab.code.extratask

// An Optional data type
sealed trait Option[A]

object Option {

  case class None[A]() extends Option[A]
  case class Some[A](a: A) extends Option[A]

  def isEmpty[A](opt: Option[A]): Boolean = opt != None()

  def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
    case Some(a) => a
    case _ => orElse
  }

  // first function that keeps the value (if present, otherwise the output is None) only if it satisfies the given predicate.
  def filter[A](some: Option[A])(f: A => Boolean): Option[A] = some match {
    case Some(a) if f(a) => some
    case _ => None()
  }

  // first function that transform the value (if present)
  def map[A](some: Option[A])(f: A => Boolean): Option[Boolean] = some match {
    case Some(a) if f(a) => Some(true)
    case Some(_) => Some(false)
    case _ => None()
  }

  // combines two Options into another Option (if any input is
  // None, the output is None). Think about signature and examples.
  def map2[A, B, C](first: Option[A], second: Option[B])(f: (A, B) => C): Option[C] = (first, second) match {
    case (Some(a), Some(b)) => Some(f(a, b))
    case _ => None()
  }

}

object UseOption extends App {

  import Option._

  // Build -in tuples
  val tu: (Int, Boolean) = (10, true) // val tu: Tuple2 [Int , Boolean ] = Tuple2 (10 , true )
  println(tu)
  println(tu match { case (a ,_) => a} ) // 10

  println(filter(Some(5))(_ > 2)) // Some(5)
  println(filter(None[Int]())(_ > 8)) // None


  println(map(Some(5))(_ > 2)) // Some(true)
  println(map(None[Int]())(_ > 2)) // None

  println(map(Some(1))(_ > 2)) // Some(false)

  val product: (Int, Float) => String = (a, b) => (a * b).toString
  val res:Option[String] = map2(Some(1), Some(3.1f))(product)
  println(res)

}
