package u03lab.code.extratask

// A generic linkedlist
sealed trait List[E]

// first companion object (i.e., module) for List
object List {

  import Option._

  case class Cons [E](head: E, tail: List[E]) extends List [E]
  case class Nil[E]() extends List[E]

  def length[E](l: List[E]): Int = l match {
    case Cons(_, t) => 1 + length(t)
    case _ => 0
  }

  def sum(l: List[Int]): Int = l match {
    case Cons(h, t) => h + sum(t)
    case _ => 0
  }

  def append[A <: C, B <: C, C](l1: List[A], l2: List[B]): List[C] = (l1, l2) match {
    case (Cons(h, t), _) => Cons[C](h, append(t, l2))
    case (_, Cons(h, t)) => Cons[C](h, append(l1, t))
    case _ => Nil()
  }

  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (Cons(_, _), 0) => l
    case (Cons(_, t), _) => drop(t, n - 1)
    case (_, _) => Nil()
  }

  def map[A,B](l: List[A])(f: A => B): List[B] = l match {
    case Cons(h, t) => Cons(f(h), map(t)(f))
    case _ => Nil()
  }

  def filter[A](l: List[A])(f: A => Boolean): List[A] = l match {
    case Cons(h, t) if f(h) => Cons(h, filter(t)(f))
    case Cons(_, t) => filter(t)(f)
    case _ => Nil()
  }

  def max(l: List[Int]): Option[Int] = l match {
    case Cons(head, tail) => filter(tail)(_ > head) match {
      case Cons(_, _) => max(tail)
      case _ => Some(head)
    }
    case _ => None()
  }

  def foldLeft[A, B](list: List[A])(base: B)(f: (B, A) => B): B = list match {
    case Cons(h, t) => f(foldLeft(t)(base)(f), h) // or also... foldLeft(t)(f(base, h))(f)
    case _ => base
  }

  def foldRight[A, B](list: List[A])(base: B)(f: (A, B) => B): B = list match {
    case Cons(h, t) => foldRight(t)(f(h, base))(f) // f(h, foldRight(t)(base)(f))
    case _ => base
  }

}


object ListTest extends App {

  import List._

  println(length(Cons("10", Cons("20", Nil())))) // 2
  println(sum(Cons(10, Cons(20, Cons(30, Nil()))))) // 60

  val l:List[Int] = Cons(10, Cons(20, Nil()))
  val l1:List[Float] = Cons(30.1f, Cons(40.4f, Nil()))
  val l2:List[AnyVal] = append(l, l1)
  val l3:List[AnyVal] = append(l1, l)
  println(l2)
  println(l3)

  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),2)) // Cons(30, Nil())
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),5)) // Nil()
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),0)) // Cons(10, Cons(20, Cons(30, Nil())))

  println(map(Cons(10, Cons(20, Nil())))(_ + 1)) // Cons(11, Cons(21, Nil()))
  println(map(Cons(10, Cons(20, Nil())))(":" + _ + ":")) // Cons(":10:", Cons(":20:", Nil()))

  println(filter(Cons(10, Cons(20, Nil())))(_ > 15)) // Cons(20, Nil())
  println(filter(Cons("first", Cons("bb", Cons("ccc", Nil()))))(_.length <= 2)) // Cons("bb", Nil())

  println(max(Cons(10, Cons(25, Cons(20, Nil()))))) // Some(25)
  println(max(Nil())) // None()

  val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
  println(foldLeft(lst)(0)(_ + _)) // 16
  println(foldRight(lst)("")(_ + _)) // "5173"

}
