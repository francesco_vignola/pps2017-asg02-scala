package u04lab.code

trait Student {
  def name: String
  def academicYear: Int
  def enrolling(course: Course): Unit // the student participates to a Course
  def enrolling(courses: Course*): Unit
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacherName?
}

trait Course {
  def name: String
  def teacherName: String
}

object Student {
  def apply(name: String, academicYear: Int = 2017): Student = StudentImpl(name, academicYear)

  private case class StudentImpl(override val name: String,
                                 override val academicYear: Int) extends Student {

     private var enrollingCourses: List[Course] = Nil()

     override def enrolling(course: Course): Unit = this.enrollingCourses = course :: this.enrollingCourses

    override def enrolling(courses: Course*): Unit = courses.foreach(course => enrolling(course))

     override def courses: List[String] = this.enrollingCourses.map(course => course.name)

     override def hasTeacher(teacher: String): Boolean =
       this.enrollingCourses.map(course => course.teacherName).contains(teacher)

  }
}

object Course {
  def apply(name: String, teacherName: String): Course = CourseImpl(name, teacherName)

  private case class CourseImpl(override val name: String,
                                 override val teacherName: String) extends Course
}

object Try extends App {
  val cPPS = Course("PPS","Viroli")
  val cPCD = Course("PCD","Ricci")
  val cSDR = Course("SDR","D'Angelo")
  val s1 = Student("mario",2015)
  val s2 = Student("gino",2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true
}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */