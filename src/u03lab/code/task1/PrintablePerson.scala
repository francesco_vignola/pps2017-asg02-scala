package u03lab.code.task1

object PrintablePerson extends App {

  def personToString(person: Person): String = person match {
    case person: Student => person.name + ": " + person.year
    case person: Teacher => person.name + ": " + "[" + person.course + "]"
  }

  println(personToString(Teacher("mirko","PPS"))) // "mirko: [PPS]"
  println(personToString(Student("mario",2016))) // "mario: 2016"

}
