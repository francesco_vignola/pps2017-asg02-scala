package u05lab.code

import scala.annotation.tailrec

object TicTacToe extends App {

  sealed trait Player {
    def other: Player = this match {case X => O; case _ => X }
    override def toString: String = this match { case X => "X"; case _ => "O" }
  }

  case object X extends Player
  case object O extends Player
  case class Mark(x: Double, y: Double, player: Player)

  type Board = List[Mark]
  type Game = List[Board]
  object EmptyBoard { def apply(): Board = List.empty[Mark] }
  object EmptyGame { def apply(): Game = List(EmptyBoard()) }

  val Movements: Int = 9

  def find(board: Board, x: Double, y: Double): Option[Player] =
    board.find(mark => mark.x == x && mark.y == y).map(_.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] = for {
    x <- 0 to 2
    y <- 0 to 2
    if find(board, x, y).isEmpty
  } yield Mark(x, y, player) :: board

  def computeAnyGame(player: Player, moves: Int): Stream[Game] =
    computeAnyGameCurried(player, moves)(updateGames)

  def computeAnyGameUntilWin(player: Player): Stream[Game] =
    computeAnyGameCurried(player, Movements)(updateGamesUntilVictory)

  private def computeAnyGameCurried(player: Player, moves: Int)(gameUpdater: (Stream[Game], Player) => Stream[Game]): Stream[Game] = {

    @tailrec
    def computeAllCombinations(currentPlayer: Player, movements: Int, games: Stream[Game]): Stream[Game] = movements match {
      case 0 => games
      case _ => computeAllCombinations(currentPlayer other, movements - 1, gameUpdater(games, currentPlayer))
    }

    computeAllCombinations(player other, moves, Stream(EmptyGame()))
  }

  private def updateGames(games: Stream[Game], player: Player): Stream[Game] =
    games flatMap (game => nextMovements(game, player))

  private def nextMovements(game: Game, player: Player): Stream[Game] =
    placeAnyMark(game.head, player).toStream.map(_ :: game)

  private def updateGamesUntilVictory(games: Stream[Game], player: Player): Stream[Game] =  {
    println("Update Game Until Victory")
    lazy val gamesOfPlayers = games.partition(game => hasWon(game, player) || hasWon(game, player other))
    gamesOfPlayers._1 #::: updateGames(gamesOfPlayers._2, player)
  }

  private def hasWon(game: Game, player: Player): Boolean = {
    val lastBoard = game.head
    val moves = lastBoard.filter(_.player == player)
    isWinning(moves)
  }

  private def isWinning(board: Board): Boolean =
    board.size >= 3 &
      (board.forall(_.x == board.head.x) ||
        board.forall(_.y == board.head.y))

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println; println("Exercise 1")
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  println; println("Exercise 2")
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  println; println("Exercise 3")
  computeAnyGame(O, 4) foreach {g => printBoards(g); println()} // take 1810
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
  println; println("Exercise 4")
  computeAnyGameUntilWin(O) foreach { g => printBoards(g); println()}
}