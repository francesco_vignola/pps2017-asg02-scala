package u04lab.code

// A person with immutable name/surname, mutable married flag, and toString
trait Person {
  def name: String
  def surname: String
  def married: Boolean
  def married_= (state: Boolean): Unit

  // a template method
  override def toString: String = name + " " + surname + " " + married
}

// easiest implementation , with overriding class parameters becoming fields
case class PersonImpl1(override val name: String,
                       override val surname: String,
                       override var married: Boolean) extends Person

// Note uniform access
object UsePerson extends App {

  val p: Person = PersonImpl1("mario", "rossi", married = false)
  println(p)

  val p1: Person = PersonImpl1("mario", "rossi", married = false)
  println(p1)

  println("p == p1? " concat (p == p1).toString)
  println("p eq p1? " concat  (p eq p1).toString)
  println("p equals p1? " concat  (p equals p1).toString)
}
