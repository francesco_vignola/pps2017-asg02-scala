package u04lab.code

import scala.annotation.tailrec
import scala.language.postfixOps // silence warnings

sealed trait List[A] {
  def head: scala.Option[A]

  def tail: scala.Option[List[A]]

  def append(list: List[A]): List[A]

  def foreach(consumer: A => Unit): Unit

  def get(pos: Int): scala.Option[A]

  def filter(predicate: A => Boolean): List[A]

  def map[B](fun: A => B): List[B]

  def toSeq: Seq[A]

  def flatMap[B](f: A => List[B]): List[B]

  def zipRight: List[(A,Int)]

  // right-associative construction: 10 :: 20 :: 30 :: Nil()
  def ::(head: A): List[A]

  def contains(element: A): Boolean

  def length: Int

  def drop(n: Int): List[A]

  def foldLeft[B](base: B)(f: (B, A) => B): B

  def foldRight[B](base: B)(f: (A, B) => B): B
}

// defining concrete implementations based on the same template
case class Cons[A](_head: A, _tail: List[A])
  extends ListImplementation[A]

case class Nil[A]()
  extends ListImplementation[A]


// enabling pattern matching on ::
object :: {
  def unapply[A](l: List[A]): scala.Option[(A,List[A])] = l match {
    case Cons(h,t) => scala.Some((h,t))
    case _ => scala.None
  }
}

// List algorithms
trait ListImplementation[A] extends List[A] {

  override def head: scala.Option[A] = this match {
    case h :: _ => scala.Some(h)
    case _ => scala.None
  }
  override def tail: scala.Option[List[A]] = this match {
    case _ :: t => scala.Some(t)
    case _ => scala.None
  }
  override def append(list: List[A]): List[A] = this match {
    case h :: t => h :: (t append list)
    case _ => list
  }
  override def foreach(consumer: A => Unit): Unit = this match {
    case h :: t => consumer(h); t foreach consumer
    case _ => None
  }
  override def get(pos: Int): scala.Option[A] = this match {
    case h :: _ if pos == 0 => scala.Some(h)
    case _ :: t if pos > 0 => t get (pos-1)
    case _ => scala.None
  }
  override def filter(predicate: A => Boolean): List[A] = this match {
    case h :: t if predicate(h) => h :: (t filter predicate)
    case _ :: t => t filter predicate
    case _ => Nil()
  }
  override def map[B](fun: A => B): List[B] = this match {
    case h :: t => fun(h) :: (t map fun)
    case _ => Nil()
  }

  override def toSeq: Seq[A] = this match {
    case h :: t => h +: t.toSeq // using method '+:' in Seq..
    case _ => Seq()
  }

  override def flatMap[B](f: A => List[B]): List[B] = this match {
    case h :: t => f(h) append t.flatMap(f)
    case _ => Nil()
  }

  override def zipRight: List[(A,Int)] = {
    @tailrec
    def zipRightTail(listToZip: List[A], zippedList: List[(A, Int)], position: Int): List[(A, Int)] = listToZip match {
      case h :: t => zipRightTail(t, zippedList append List((h, position)), position + 1)
      case _ => zippedList
    }

    zipRightTail(this, Nil(), 0)
  }

  override def ::(head: A): List[A] = Cons(head,this)

  override def contains(element: A): Boolean = this.filter(_ == element) != Nil()

  override def length: Int = this match {
    case _ :: t => 1 + t.length
    case _ => 0
  }

  override def drop(n: Int): List[A] = (this, n) match {
    case (_, i) if i <= 0 => this
    case (_ :: t, i) => t.drop(i - 1)
    case (_, _) => this // or Nil()
  }

  def foldLeft[B](base: B)(f: (B, A) => B): B = this match {
    case h :: t => f(t.foldLeft(base)(f), h) // or also... foldLeft(t)(f(base, h))(f)
    case _ => base
  }

  def foldRight[B](base: B)(f: (A, B) => B): B = this match {
    case Cons(h, t) => t.foldRight(f(h, base))(f) // f(h, foldRight(t)(base)(f))
    case _ => base
  }

}

// Factories
object List {

  def apply[A](elems: A*): List[A] = {
    var list: List[A] = Nil()
    for (i <- elems.length-1 to 0 by -1) list = elems(i) :: list
    list
  }

  def of[A](elem: A, n: Int): List[A] =
    if (n==0) Nil() else elem :: of(elem,n-1)
}

object Test extends App {

  import List._  // Working with the above lists
  print("List: ")
  println(List(10,20,30,40))

  val l = 10 :: 20 :: 30 :: 40 :: Nil() // same as above
  print("Head: ")
  println(l.head) // 10

  print("Tail: ")
  println(l.tail) // 20,30,40

  print("List resulting from append: ")
  println(l append l) // 10,20,30,40,10,20,30,40

  print("Seq resulting from append: ")
  println(l append l toSeq) // as a list: 10,20,30,40,10,20,30,40

  print("Element at index 2: ")
  println(l get 2) // 30

  print("List of 10 \"a\": ")
  println(of("a",10)) // a,a,a,..,a

  print("List resulting from filter and map: ")
  println(l filter (_<=20) map ("a"+_)) // a10, a20

  println()
  println("Seq resulting from flatMap: ")
  println(l flatMap(i => i+"a" :: i+"b" :: Nil()) toSeq) // List(10a, 10b, 20a, 20b, 30a, 30b, 40a, 40b)
  println(l flatMap(i => i+1 :: Nil()) toSeq) // List(11, 21, 31, 41)
  println(l flatMap(i => of(i,i/10)) toSeq) // List(10, 20, 20, 30, 30, 30, 40, 40, 40, 40)

  println()
  print("Seq resulting from zipRight: ")
  println(l.zipRight.toSeq) // List((10,0), (20,1), (30,2), (40,3))

  print("Length of list: ")
  println(l.length)

  print("List resulting from drop: ")
  println(l.drop(1))

  print("FoldLeft: ")
  println(l.foldLeft(0)(_ + _))

  print("FoldRight: ")
  println(l.foldRight("")(_ + _))
}

/** Hints:
  * - move implementation of :: from trait List to ListImplementation
  * - play a bit with already implemented methods
  * - implement flatMap: it maps each element into a list, and flattens the whole list of lists into a list (use append) -- the solution is 2 lines!
  * - implement zipRight: creates a list of pairs, with the second element being 0,1,2,3,.. -- may need an internal recursion
  */