package u03lab.code.task2

import u03lab.code.task1.{Person, Student, Teacher}

object ListOfPersonTest extends App {

  import List._

  def keepAllCourses(people: List[Person]): List[String] =
    map(filter(people)(_.isInstanceOf[Teacher]))(_.asInstanceOf[Teacher].course)

  val list: List[Person] = Cons(Student("Mario", 2016), Cons(Teacher("Mirko", "PPS"), Cons(Teacher("Alessandro", "PCD"), Cons(Student("Francesco", 2017), Nil()))))
  println(keepAllCourses(list))
}