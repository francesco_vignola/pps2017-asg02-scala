package u04lab.code

trait Complex {
  def real: Double
  def image: Double
  def +(complexNumber: Complex): Complex
  def *(complexNumber: Complex): Complex
}

/*  Implementation of ComplexImpl as a simple class
object Complex {

  def apply(real: Double, image: Double):Complex = new ComplexImpl(real, image)

  private class ComplexImpl(override val real: Double,
                                 override val image: Double) extends Complex {

     override def +(complexNumber: Complex): Complex =
        new ComplexImpl(this.real + complexNumber.real, this.image + complexNumber.image)

     override def *(complexNumber: Complex): Complex =
        new ComplexImpl((this.real * complexNumber.real) - (this.image * complexNumber.image),
           (this.real * complexNumber.image) + (this.image * complexNumber.real))

  }
}
*/

// Implementation of ComplexImpl as a case class
object Complex {

  def apply(real: Double, image: Double): Complex = ComplexImpl(real, image)

  private case class ComplexImpl(override val real: Double,
                            override val image: Double) extends Complex {

     override def +(complexNumber: Complex): Complex =
        ComplexImpl(this.real + complexNumber.real, this.image + complexNumber.image)

     override def *(complexNumber: Complex): Complex =
        ComplexImpl((this.real * complexNumber.real) - (this.image * complexNumber.image),
           (this.real * complexNumber.image) + (this.image * complexNumber.real))

  }

}

object TryComplex extends App {
  val a = Array(Complex(10,20), Complex(1,1), Complex(7,0))
  val c = a(0)+a(1)+a(2)
  println(c, c.real, c.image)

  val c2 = a(0)*a(1)
  println(c2, c2.real, c2.image)

/*  Check of the objects equality, of the references equality and of the
    toString both for the implementation as a class and as a case class

  println("Test object equality: c == c2 ? " + (c == c2).toString)
  println("Test object equality: c equals c2 ? " + (c equals c2).toString)
  println("Test reference equality: c eq c2 ? " + (c eq c2).toString)
*/
}

/** Hints:
  * - implement Complex with a ComplexImpl class, similar to PersonImpl in slides
  * - check that equality and toString do not work
  * - use a case class ComplexImpl instead, creating objects without the 'new' keyword
  * - check equality and toString now
  */