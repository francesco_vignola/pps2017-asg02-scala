package u03lab.code.extratask

sealed trait Either[+L, +R]

object Either {

  case class Left[+L](error: L) extends Either[L, Nothing]
  case class Right[+R](success: R) extends Either[Nothing, R]

  def toString[A, B](e: Either[A, B]): String = e match {
    case Left(a) => a.toString
    case Right(b) => b.toString
  }

  def isRight[A, B](e: Either[A, B]): Boolean = e match {
    case Left(_) => false
    case Right(_) => true
  }

  def isLeft[A, B](e: Either[A, B]): Boolean = e match {
    case Left(_) => true
    case Right(_) => false
  }

}

object EitherTest extends App {

  import Either._

  def validateName(name: String): Either[String, String] = name match {
    case "" => Left("Name cannot be empty")
    case _ => Right(name)
  }

  println(Either.toString(validateName("")))
  println(Either.toString(validateName("Francesco")))

  println(Either.isRight(validateName("")))
  println(Either.isRight(validateName("Francesco")))
}
