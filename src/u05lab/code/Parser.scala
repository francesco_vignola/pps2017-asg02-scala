package u05lab.code

/** Consider the Parser example shown in previous lesson.
  * Analogously to NonEmpty, create a mixin NotTwoConsecutive,
  * which adds the idea that one cannot parse two consecutive
  * elements which are equal.
  * Use it (as a mixin) to build class NotTwoConsecutiveParser,
  * used in the testing code at the end.
  * Note we also test that the two mixins can work together!!
  */

abstract class Parser[T] {
  def parse(element: T): Boolean
  def end(): Boolean
  def parseAll(elements: Seq[T]): Boolean = (elements forall parse) & end()
}

case class BasicParser(elements: Set[Char]) extends Parser[Char] {
  override def parse(element: Char): Boolean = elements.contains(element)
  override def end(): Boolean = true
}

trait NonEmpty[T] extends Parser[T]{
  private[this] var empty = true
  abstract override def parse(element: T): Boolean = {empty = false; super.parse(element)}
  abstract override def end(): Boolean = !empty && {empty = true; super.end()}
}

class NonEmptyParser(elements: Set[Char]) extends BasicParser(elements) with NonEmpty[Char]

trait NotTwoConsecutive[T] extends Parser[T]{
  private[this] var element: T = _
  private[this] var consecutive = true

  abstract override def parse(t: T): Boolean = {
    val consecutive: Boolean = super.parse(t) && t == element
    element = t; !consecutive
  }

  abstract override def end(): Boolean = consecutive && {consecutive = true; super.end()}
}

class NotTwoConsecutiveParser(elements: Set[Char]) extends BasicParser(elements) with NotTwoConsecutive[Char]


object TryParsers extends App {

  val parser = BasicParser(Set('a', 'b', 'c'))
  println("Basic")
  println(parser.parseAll("aabc".toList)) // true
  println(parser.parseAll("aabcdc".toList)) // false
  println(parser.parseAll("".toList)) // true

  val parserNE = new NonEmptyParser(Set('0','1'))
  println()
  println("NonEmpty")
  println(parserNE.parseAll("0101".toList)) // true
  println(parserNE.parseAll("0123".toList)) // false
  println(parserNE.parseAll(List())) // false

  val parserNTC = new NotTwoConsecutiveParser(Set('X','Y','Z'))
  println()
  println("NotTwoConsecutive")
  println(parserNTC.parseAll("XYZ".toList)) // true
  println(parserNTC.parseAll("XYYZ".toList)) // false
  println(parserNTC.parseAll("".toList)) // true

  val parserNTCNE = new BasicParser(Set('X','Y','Z')) with NotTwoConsecutive[Char] with NonEmpty[Char]
  println()
  println("NotTwoConsecutive and NonEmpty")
  println(parserNTCNE.parseAll("XYZ".toList)) // true
  println(parserNTCNE.parseAll("XYYZ".toList)) // false
  println(parserNTCNE.parseAll("".toList)) // false

}