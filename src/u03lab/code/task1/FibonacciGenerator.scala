package u03lab.code.task1

import scala.annotation.tailrec

object FibonacciGenerator extends App {

  def fibonacci(number: Int): Int = number match {
    case 0 => 0
    case 1 => 1
    case _  => fibonacci(number - 1) + fibonacci(number - 2)
  }

  // Alternative solution with tail recursion
  def fibonacci_tail(number: Int): Int = {

    @tailrec
    def fibonacci_rec(number: Int, fibonacci1: Int, fibonacci2: Int): Int = number match {
      case 0 => fibonacci1
      case 1 => fibonacci2
      case _ => fibonacci_rec(number - 1, fibonacci2, fibonacci1 + fibonacci2)
    }

    fibonacci_rec(number, 0, 1)
  }

  println(fibonacci(0), fibonacci(1), fibonacci(2), fibonacci(3)) // (0,1,1,2)

  println(fibonacci(0), fibonacci(1), fibonacci(2), fibonacci(3), fibonacci(4), fibonacci(5), fibonacci(6)) // (0,1,1,2, 3, 5, 8)
  println(fibonacci_tail(0), fibonacci_tail(1), fibonacci_tail(2), fibonacci_tail(3), fibonacci_tail(4), fibonacci_tail(5), fibonacci_tail(6)) // (0,1,1,2, 3, 5, 8)

  println(fibonacci(42))  // 267914296
  println(fibonacci_tail(42)) // 267914296

}
